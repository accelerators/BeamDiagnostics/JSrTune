/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRTune;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.EnumScalarEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IEnumScalar;
import fr.esrf.tangoatk.core.IEnumScalarListener;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.INumberScalarListener;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.IRefresherListener;
import fr.esrf.tangoatk.core.ISpectrumListener;
import fr.esrf.tangoatk.core.NumberScalarEvent;
import fr.esrf.tangoatk.core.NumberSpectrumEvent;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.chart.IJLChartListener;
import fr.esrf.tangoatk.widget.util.chart.JLAxis;
import fr.esrf.tangoatk.widget.util.chart.JLChart;
import fr.esrf.tangoatk.widget.util.chart.JLChartEvent;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

/**
 *
 * @author pons
 */

class Plane implements ISpectrumListener, INumberScalarListener {

  TuneViewer parent;

  INumberSpectrum spectrumModel = null;
  INumberSpectrum spectrumRefModel = null;
  INumberScalar maxIndexModel = null;
  INumberScalar spanModel = null;
  INumberScalar tuneModel = null;
  JLDataView dvy;
  JLDataView dvyRef;
  JLDataView spanDv;
  JLDataView tuneDv;
  double maxIndex;
  double trackSpan;
  boolean trackVisible;
  boolean visible;
  boolean refVisible;
  boolean autoVisible;
  double A0 = 0.0;
  double A1 = 1.0;
  boolean setPeakMode;
  double[] reference;
  long lastUpdate;

  Plane(TuneViewer parent, String name, Color color, Color refColor) {

    this.parent = parent;
    maxIndex = -1;
    trackSpan = Double.NaN;
    trackVisible = false;
    refVisible = false;
    visible = true;
    setPeakMode = false;
    reference = null;

    dvy = new JLDataView();
    dvy.setColor(color);
    dvy.setName(name);
    dvyRef = new JLDataView();
    dvyRef.setColor(refColor);
    dvyRef.setName(name+"_Ref");
    tuneDv = new JLDataView();
    tuneDv.setColor(refColor);
    tuneDv.setLabelVisible(false);
    tuneDv.setClickable(false);
    tuneDv.setLineWidth(2);
    tuneDv.setStyle(JLDataView.STYLE_DASH);
    spanDv = new JLDataView();
    spanDv.setColor(Color.BLACK);
    spanDv.setClickable(false);
    spanDv.setMarker(JLDataView.MARKER_SQUARE);
    spanDv.setMarkerColor(color.darker());
    spanDv.setLabelVisible(false);
    lastUpdate = System.currentTimeMillis();

  }

  public void clearModel() {

    if (spectrumModel != null) {
      spectrumModel.removeSpectrumListener(this);
    }
    if (maxIndexModel != null) {
      maxIndexModel.removeNumberScalarListener(this);
    }
    if (spanModel != null) {
      spanModel.removeNumberScalarListener(this);
    }
    spectrumModel = null;
    maxIndexModel = null;
    spanModel = null;

  }

  public void setModel(INumberSpectrum spectrum, INumberSpectrum spectrumRef,
          INumberScalar maxIndex, INumberScalar span,INumberScalar tune) {

    clearModel();
    spectrumModel = spectrum;
    if (spectrumModel != null) {
      spectrumModel.addSpectrumListener(this);
    }
    spectrumRefModel = spectrumRef;
    if (spectrumRefModel != null) {
      spectrumRefModel.addSpectrumListener(this);
    }
    maxIndexModel = maxIndex;
    if (maxIndexModel != null) {
      maxIndexModel.addNumberScalarListener(this);
    }
    spanModel = span;
    if (spanModel != null) {
      spanModel.addNumberScalarListener(this);
    }
    tuneModel = tune;
    if (tuneModel != null) {
      tuneModel.addNumberScalarListener(this);
    }

  }

  public void errorChange(ErrorEvent errorEvent) {

    Object src = errorEvent.getSource();

    if (src == maxIndexModel) {
      maxIndex = Double.NaN;
    } else if (src == spanModel) {
      trackSpan = Double.NaN;
    } else if (src == spectrumModel) {
      dvy.reset();
    } else if (src == tuneModel) {
      tuneDv.reset();
    }

    updatePeak();

  }
  
  public void refresh() {
    
    spectrumModel.refresh();
    spectrumRefModel.refresh();

    if(maxIndexModel!=null)
      maxIndexModel.refresh();
    if(spanModel!=null)
      spanModel.refresh();
    
  }

  public void stateChange(AttributeStateEvent evt) {
  }

  public void spectrumChange(NumberSpectrumEvent numberSpectrumEvent) {

    
    if(!parent.isVisible())
      return;
      
    Object src = numberSpectrumEvent.getSource();
    
    if (src == spectrumModel) {
      
      double[] value = numberSpectrumEvent.getValue();
      int length = value.length;

      synchronized (dvy) {
        parent.decimate = 1;
        if(MainPanel.doDecimation) {
          int nbPoint = parent.maxIdx - parent.minIdx;
          if(nbPoint>2048) parent.decimate = nbPoint / 2048;
        }
        dvy.reset();
        for (int i = parent.minIdx; i < parent.maxIdx; i+=parent.decimate) {
          dvy.add(A0 + A1 * (double) i, value[i], false);
        }
        dvy.updateFilters();
      }

      updatePeak();
      
      long now = System.currentTimeMillis();
      long updateTime = (now-lastUpdate);
      //System.out.println(parent.getTypeName()+" " + dvy.getName() + ": "+updateTime+"ms");
      lastUpdate=now;
      
      parent.doRepaint(this);

    } else if (src == spectrumRefModel) {

      double[] value = numberSpectrumEvent.getValue();
      int length = value.length;

      synchronized (dvyRef) {
        dvyRef.reset();
        for (int i = parent.minIdx; i < parent.maxIdx; i+=parent.decimate) {
          dvyRef.add(A0 + A1 * (double) i, value[i], false);
        }
        dvyRef.updateFilters();
      }      

    }

  }
  
  public void numberScalarChange(NumberScalarEvent nse) {

    
    Object src = nse.getSource();
    if (src == maxIndexModel) {      
      maxIndex = nse.getValue();
    } else if (src == spanModel) {
      trackSpan = nse.getValue();
    } else if (src == tuneModel) {
      updateTune(nse.getValue());
    }
    
    updatePeak();
    
  }
  
  void updateTune(double tune) {
    
    tuneDv.reset();
    if(Double.isNaN(tune))
      return;

    double t;
    
    if (parent.getXAxisMode() == TuneViewer.FREQUENCY) {
     t = tune * (TuneViewer.REVOLUTION_FREQ / 1000.0);
    } else {
     t = tune; 
    }
    
    tuneDv.add(t,1e-10);
    tuneDv.add(t,1e+10);
    
  }

  void updatePeak() {

    spanDv.reset();
    if (Double.isNaN(maxIndex) || Double.isNaN(trackSpan) || !trackVisible || !visible ||
        maxIndex<parent.minIdx || maxIndex>=parent.maxIdx || setPeakMode ) {
      return;
    }

    double width;
    double center;
    double height = 0;

    if (parent.getXAxisMode() == TuneViewer.FREQUENCY) {
      width = trackSpan;
      center = (maxIndex / TuneViewer.SPECTRUM_WIDTH) * (TuneViewer.REVOLUTION_FREQ / 2000.0);
    } else {
      width = (trackSpan * 1000.0) / TuneViewer.REVOLUTION_FREQ;
      center = (maxIndex / TuneViewer.SPECTRUM_WIDTH) / 2.0;
    }

    height = dvy.getYValueByIndex((int)(maxIndex- parent.minIdx)/parent.decimate );

    spanDv.add(center - width / 2.0, height);
    spanDv.add(center, height);
    spanDv.add(center + width / 2.0, height);

  }
  
  void takeRef() {
      
    try {
   
      DeviceProxy ds = (DeviceProxy)spectrumModel.getDevice();
      DeviceAttribute src = ds.read_attribute(spectrumModel.getNameSansDevice());
      DeviceAttribute dst = new DeviceAttribute(spectrumRefModel.getNameSansDevice());
      dst.insert(src.extractDoubleArray());
      ds.write_attribute(dst);
      
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(parent,"Reference Error" , spectrumRefModel.getName(), e);
    }
    
  }

}

public class TuneViewer extends JLChart implements INumberScalarListener,IEnumScalarListener,IJLChartListener,IRefresherListener {

  public final static double REVOLUTION_FREQ = 355212.8609;  
  public final static double SPECTRUM_WIDTH = 16383.0;
  
  public final static int FRACTIONNAL = 0;
  public final static int FREQUENCY = 1;
  
  public final static int TYPE_FFT   = 0;
  public final static int TYPE_FFTC  = 1;
  public final static int TYPE_PHASE = 2;
  
  public final static Font chartFont = new Font("Dialog",Font.PLAIN,16);

  
  private MainPanel parent;
  private int mode = FREQUENCY;
  private int type;
  private int hType;
  private int vType;
  private IEnumScalar hModeModel=null;
  private IEnumScalar vModeModel=null;
  private INumberScalar minXModel=null;
  private INumberScalar minYModel=null;
  private INumberScalar maxXModel=null;
  private INumberScalar maxYModel=null;
  
  private Plane hPlane;
  private Plane vPlane;
  
  int minIdx=0;
  int maxIdx=(int)SPECTRUM_WIDTH;
  int decimate = 1;
  
  double minX=Double.NaN;
  double minY=Double.NaN;
  double maxX=Double.NaN;
  double maxY=Double.NaN;
    
  public TuneViewer(MainPanel parent,int type) {

    this.type = type;
    this.parent = parent;
    
    setBorder(new javax.swing.border.EtchedBorder());
    setBackground(new java.awt.Color(180, 180, 180));
    
    getY1Axis().setAutoScale(type==TYPE_PHASE);
    getXAxis().setAutoScale(true);
    getXAxis().setAnnotation(JLAxis.VALUE_ANNO);
    
    getXAxis().setFont(chartFont);
    getY1Axis().setFont(chartFont);
    
    getXAxis().setGridVisible(true);
    getY1Axis().setGridVisible(true);
    getXAxis().setLabelOffset(0, 3);
    
    hPlane = new Plane(this,"Horizontal",Color.RED,new Color(255,180,180));
    vPlane = new Plane(this,"Vertical",Color.BLUE,new Color(180,180,255));
    
    getY1Axis().addDataView(hPlane.dvy);
    getY1Axis().addDataView(hPlane.spanDv);
    getY1Axis().addDataView(hPlane.tuneDv);
    
    getY1Axis().addDataView(vPlane.dvy);
    getY1Axis().addDataView(vPlane.spanDv);    
    getY1Axis().addDataView(vPlane.tuneDv);
    
    setBackground(Color.WHITE);
    setPreferredSize(new Dimension(1024,500));

    if(type==TYPE_FFT ) {
      getY1Axis().setName("Magnitude (a.u.)");     
      setLabelVisible(false);
    } else if (type==TYPE_FFTC) {
      getY1Axis().setName("Correlated Magnitude (a.u.)");     
      setLabelVisible(true);
    } else {
      getY1Axis().setName("Phase (rad)");           
    }
    
    getXAxis().setName("Frequency (KHz)");
    
    setJLChartListener(this);
        
  }
  
  public void setRange(double fMin,double fMax,double minY,double maxY) {
    
    if( !Double.isNaN(fMin) && !Double.isNaN(fMax)) {
      
      int mnIdx = (int)( (fMin*2000.0/REVOLUTION_FREQ) * SPECTRUM_WIDTH );
      int mxIdx = (int)( (fMax*2000.0/REVOLUTION_FREQ) * SPECTRUM_WIDTH );
      minIdx = mnIdx;
      maxIdx = mxIdx;    
      if(minIdx<0) minIdx=0;
      if(maxIdx>(int)SPECTRUM_WIDTH) maxIdx = (int)SPECTRUM_WIDTH;
      
    }
    
    if(!Double.isNaN(minY)) getY1Axis().setMinimum(minY);
    if(!Double.isNaN(maxY)) getY1Axis().setMaximum(maxY);

  }
  
  public void setScaleModel(INumberScalar minX,INumberScalar maxX,INumberScalar minY,INumberScalar maxY) {

    minXModel = minX;
    minYModel = minY;
    maxXModel = maxX;
    maxYModel = maxY;    
    if(minXModel!=null) minXModel.addNumberScalarListener(this);
    if(minYModel!=null) minYModel.addNumberScalarListener(this);
    if(maxXModel!=null) maxXModel.addNumberScalarListener(this);
    if(maxYModel!=null) maxYModel.addNumberScalarListener(this);    
    
  }
  
  public void setHModel(INumberSpectrum spectrum, INumberSpectrum spectrumRef, 
          INumberScalar maxIndex, INumberScalar span, IEnumScalar mode,INumberScalar tune) {
    hPlane.setModel(spectrum, spectrumRef, maxIndex, span, tune);
    hModeModel = mode;
    if(mode!=null)
      hModeModel.addEnumScalarListener(this);
  }
  
  public void setVModel(INumberSpectrum spectrum, INumberSpectrum spectrumRef, 
          INumberScalar maxIndex, INumberScalar span, IEnumScalar mode,INumberScalar tune) {
    vPlane.setModel(spectrum, spectrumRef, maxIndex, span, tune);    
    vModeModel = mode;
    if(mode!=null)
      vModeModel.addEnumScalarListener(this);
  }
  
  public void takeRef() {
    hPlane.takeRef();
    vPlane.takeRef();
  }
  
  public void clearModel() {
    hPlane.clearModel();
    vPlane.clearModel();
    if(hModeModel!=null) hModeModel.removeEnumScalarListener(this);
    if(vModeModel!=null) vModeModel.removeEnumScalarListener(this);      
    if(minXModel!=null) minXModel.removeNumberScalarListener(this);      
    if(maxXModel!=null) maxXModel.removeNumberScalarListener(this);      
    if(minYModel!=null) minYModel.removeNumberScalarListener(this);      
    if(maxYModel!=null) maxYModel.removeNumberScalarListener(this);    
  }
  
  public void refresh() {
    // Force refresh
    vPlane.refresh();
    hPlane.refresh();
  }
  
  public int getXAxisMode() {
    return mode;
  }

  public void setXAxisMode(int mode) {
    
    this.mode = mode;
    double a1 = 1.0;
    
    switch(mode) {
      
      case FRACTIONNAL:
        a1 = (1.0/16383.0)/2.0;
        getXAxis().setName("Fractionnal");
        break;
        
      case FREQUENCY:
        a1 = (REVOLUTION_FREQ/SPECTRUM_WIDTH)/2000.0;      
        getXAxis().setName("Frequency (KHz)");
        break;
        
    }
    
    hPlane.A1 = a1;
    vPlane.A1 = a1;
    vPlane.updatePeak();
    hPlane.updatePeak();
    repaint();

  }
  
  public void setHPeak() {
    if(type==hType) {
      hPlane.setPeakMode = true;
      hPlane.updatePeak();
      repaint();
    }
  }

  public void setVPeak() {
    if(type==vType) {
      vPlane.setPeakMode = true;
      vPlane.updatePeak();
      repaint();
    }
  }
  
  private void setTrackVisible(Plane p,boolean visible) {
    
    if(visible==p.trackVisible)
      return;
    
    p.trackVisible = visible;
    p.updatePeak();
    repaint();
    
  }
  
  private void setRefVisible(Plane p,boolean visible) {

    if(visible==p.refVisible)
      return;

    p.refVisible = visible;
    if(!visible)
      p.dvyRef.removeFromAxis();
    else
      getY1Axis().addDataView(p.dvyRef); 
    
  }

  void setRefVisible(boolean visible) {
    setRefVisible(hPlane,visible);
    setRefVisible(vPlane,visible);    
    repaint();
  }
      
  private void setPlaneVisible(Plane p,boolean visible) {
    
    if(visible==p.visible)
      return;

    p.visible = visible;

    if(!visible) {
      p.dvy.removeFromAxis();
      p.dvyRef.removeFromAxis();
      p.tuneDv.removeFromAxis();
      setTrackVisible(p,false);
    } else {
      getY1Axis().addDataView(p.dvy);
      getY1Axis().addDataView(p.tuneDv);
      if(p.refVisible) getY1Axis().addDataView(p.dvyRef);
      // Track will be readded automaticaly according to the mode
    }
    repaint();
    
  }
  
  void doRepaint(Plane src) {

    boolean vH = hPlane.visible;
    boolean vV = vPlane.visible;
    
    if( vH ) {
      if(src==hPlane)
        repaint();
    } else if ( vV ) {
      if(src==vPlane)
        repaint();
    }
    
  }
  
  public void setHVisible(boolean visible) {
    setPlaneVisible(hPlane,visible);
  }
          
  public void setVVisible(boolean visible) {
    setPlaneVisible(vPlane,visible);
  }  

  @Override
  public void numberScalarChange(NumberScalarEvent nse) {
    
    INumberScalar src = nse.getNumberSource();
    
    if(src==minXModel) {
      minX = nse.getDeviceValue();
    } else if (src==maxXModel) {
      maxX = nse.getDeviceValue();
    } else if (src==minYModel) {
      minY = nse.getDeviceValue();
    } else if (src==maxYModel) {
      maxY = nse.getDeviceValue();
    }
            
  }
  
  @Override
  public void enumScalarChange(EnumScalarEvent ese) {
    
    Object src = ese.getSource();
    
    if(src==hModeModel) {
      
      hType = hModeModel.getShortValueFromEnumScalar(ese.getValue());
      setTrackVisible(hPlane,hType==type && mode<2);

    } else if (src==vModeModel) {
      
      vType = vModeModel.getShortValueFromEnumScalar(ese.getValue());
      setTrackVisible(vPlane,vType==type && mode<2);
            
    }
    
  }

  @Override
  public void stateChange(AttributeStateEvent ase) {
  }

  @Override
  public void errorChange(ErrorEvent ee) {
  }
  
  public String[] clickOnChart(JLChartEvent jlce) {

    String[] ret = new String[2];
    JLDataView src = jlce.getDataView();
    
    
    if(src==hPlane.dvy && hPlane.setPeakMode) {
      
        ret[0] = "Peak H Marker";
        ret[1] = "MaxIndex=" + jlce.getDataViewIndex();
        hPlane.maxIndexModel.setValue(jlce.getDataViewIndex()*decimate+minIdx);
        hPlane.setPeakMode = false;
        parent.resetPeakMarker();
        return ret;
              
    }
    
    if(src==vPlane.dvy && vPlane.setPeakMode) {
      
        ret[0] = "Peak V Marker";
        ret[1] = "MaxIndex=" + jlce.getDataViewIndex();
        vPlane.maxIndexModel.setValue(jlce.getDataViewIndex()*decimate+minIdx);
        vPlane.setPeakMode = false;
        parent.resetPeakMarker();
        return ret;
              
    }
        
    if(mode==FREQUENCY)
      ret[0] = String.format( "%.1f" , jlce.getXValue() ) + " KHz";
    else
      ret[0] = String.format( "%.3f" , jlce.getXValue() );
    
    if(type==TYPE_FFT || type==TYPE_FFTC) {
      ret[1] = String.format( "%.3f", jlce.getYValue() ) + " mm²";     
    } else {
      ret[1] = String.format( "%.3f", jlce.getYValue() ) + " rad";            
    }
    
    return ret;
    
  }
  
  public String getTypeName() {
    
    switch(type) {
      case TYPE_FFT:
        return "FFT";
      case TYPE_FFTC:
        return "FFTC";
      case TYPE_PHASE:
        return "PHASE";
      default:
        return "UNKNOWN";
    }
    
  }

  
  @Override
  public void refreshStep() {
    setRange(minX,maxX,minY,maxY);
  }

}
