/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRTune;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributePolledList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IEnumScalar;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.widget.util.ATKDiagnostic;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.SettingsManagerProxy;
import fr.esrf.tangoatk.widget.util.Splash;
import fr.esrf.tangoatk.widget.util.chart.JLAxis;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Vector;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


/**
 *n
 * @author pons
 */
public class MainPanel extends javax.swing.JFrame implements MouseListener,ActionListener {

  static final String APP_RELEASE = "3.2";
  static Splash splash;
  
  static String hTuneDeviceName = null;
  static String vTuneDeviceName = null;
  static String shakerManager = "srdiag/tm/shakers";
  static String settingManagerName = "sys/settings/sr-tune";
  static String tuneSelectorName = "srdiag/beam-tune/main";
  static String machstatName = "sys/machstat/tango";
  
  static String hSparkDeviceName;
  static String vSparkDeviceName;
  static public ErrorHistory errWin;
  static boolean doDecimation = true;
  
  private TuneFitFrame hFitFrame;

  private boolean runningFromShell = true;
  
  private AttributeList attList;
  private TuneViewer fftPanel;
  private TuneViewer fftCPanel;
  private TuneViewer phasePanel;
  private SettingsManagerProxy sm;
  
  private AttributeList fftAttList;

  private AttributePolledList fftRangeAttList;
  
  private SettingsFrame hFrame = null;
  private SettingsFrame vFrame = null;
  private TuneAdjustFrame tFrame = null;
  private SynchFrame synchFrame = null;
  private RangeFrame rFrame;
  private Database db;
  
  class Apps {
    String    name;
    String    command;
    JMenuItem menuItem;            
  }
  
  private Vector<Apps> applications;

  /**
   * Creates new form MainPanel
   */
  public MainPanel(boolean runningFromShell,boolean isSpare) {
    
    // Handle windowClosing (Close from the system menu)
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitForm();
      }
    });
    
    // Swing init
    
    initComponents();
    
    // Global variables
    
    this.runningFromShell = runningFromShell;
    
    errWin = new ErrorHistory();    
    rFrame = new RangeFrame(this);

    // Splash window
    
    splash = new Splash();
    splash.setTitle("SR Tune " + APP_RELEASE);
    splash.setCopyright("(c) ESRF 2013");
    splash.setMaxProgress(100);
    splash.progress(0);
    
    try {
      db = ApiUtil.get_db_obj();
      DbDatum hdatum = db.get_device_property(MainPanel.hTuneDeviceName, "SparkName");
      hSparkDeviceName = hdatum.extractString();
      DbDatum vdatum = db.get_device_property(MainPanel.vTuneDeviceName, "SparkName");
      vSparkDeviceName = vdatum.extractString();
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(null, "Database", e);
    }

    int modeH=0;
    int modeV=0;
    
    try {
      DeviceProxy dsH = new DeviceProxy(hTuneDeviceName);
      DeviceAttribute daH = dsH.read_attribute("Mode");
      modeH = daH.extractShort();            
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(this, hTuneDeviceName, e);
    }
    
    try {
      DeviceProxy dsV = new DeviceProxy(vTuneDeviceName);
      DeviceAttribute daV = dsV.read_attribute("Mode");
      modeV = daV.extractShort();      
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(this,vTuneDeviceName, e);
    }
    
    boolean fftVisible = modeH==0 || modeV==0;
    boolean fftCVisible = modeH==1 || modeV==1 || modeH==2 || modeV ==2;
      

    fftPanel = new TuneViewer(this,TuneViewer.TYPE_FFT);
    fftPanel.getY1Axis().setScale(JLAxis.LOG_SCALE);
    fftPanel.setXAxisMode(TuneViewer.FREQUENCY);        
    fftPanel.setVisible(fftVisible);
    displayFFTCheckBox.setSelected(fftVisible);

    fftCPanel = new TuneViewer(this,TuneViewer.TYPE_FFTC);
    fftCPanel.getY1Axis().setScale(JLAxis.LOG_SCALE);
    fftCPanel.setXAxisMode(TuneViewer.FREQUENCY);   
    fftCPanel.setVisible(fftCVisible);
    displayFFTCorrelatedCheckBox.setSelected(fftCVisible);

    phasePanel = new TuneViewer(this,TuneViewer.TYPE_PHASE);
    phasePanel.setXAxisMode(TuneViewer.FREQUENCY);        
    phasePanel.setVisible(false);
     
    fftAttList = new AttributeList();
    fftAttList.addErrorListener(MainPanel.errWin);
    
    fftRangeAttList = new AttributePolledList();
    fftRangeAttList.addErrorListener(MainPanel.errWin);
    fftRangeAttList.setForceRefresh(false);    
    fftRangeAttList.addRefresherListener(fftPanel);
    fftRangeAttList.addRefresherListener(fftCPanel);
    fftRangeAttList.addRefresherListener(phasePanel);      

    attList = new AttributeList();
    attList.addErrorListener(MainPanel.errWin);

    INumberScalar hTuneModel=null;
    INumberScalar vTuneModel=null;
            
    try {

      // Horizontal tune for chart scale model
      INumberScalar minXModel = (INumberScalar)fftRangeAttList.add(MainPanel.hTuneDeviceName+"/minX");
      INumberScalar minYModel = (INumberScalar)fftRangeAttList.add(MainPanel.hTuneDeviceName+"/minY");
      INumberScalar maxXModel = (INumberScalar)fftRangeAttList.add(MainPanel.hTuneDeviceName+"/maxX");
      INumberScalar maxYModel = (INumberScalar)fftRangeAttList.add(MainPanel.hTuneDeviceName+"/maxY");
            
      fftPanel.setScaleModel(minXModel, maxXModel, minYModel, maxYModel);
      fftCPanel.setScaleModel(minXModel, maxXModel, minYModel, maxYModel);
      rFrame.setScaleModel(minXModel, maxXModel, minYModel, maxYModel);
      phasePanel.setScaleModel(minXModel, maxXModel, null, null);      
            
      IEnumScalar   hModeModel = (IEnumScalar)fftRangeAttList.add(MainPanel.hTuneDeviceName+"/Mode");
      INumberScalar hMaxIndexModel = (INumberScalar)fftAttList.add(MainPanel.hTuneDeviceName+"/MaxIndex");
      INumberScalar hTrackSpanModel = (INumberScalar)fftAttList.add(MainPanel.hTuneDeviceName+"/TrackSpan");
      INumberSpectrum hFFTMode = (INumberSpectrum)fftAttList.add(MainPanel.hTuneDeviceName+"/FFT");
      INumberSpectrum hFFTModeRef = (INumberSpectrum)fftAttList.add(MainPanel.hTuneDeviceName+"/FFT_Ref");
      INumberSpectrum hFFTCMode = (INumberSpectrum)fftAttList.add(MainPanel.hTuneDeviceName+"/FFTC");
      INumberSpectrum hFFTCModeRef = (INumberSpectrum)fftAttList.add(MainPanel.hTuneDeviceName+"/FFTC_Ref");
      INumberSpectrum hPhaseMode = (INumberSpectrum)fftAttList.add(MainPanel.hTuneDeviceName+"/PhaseC");
      INumberSpectrum hPhaseModeRef = (INumberSpectrum)fftAttList.add(MainPanel.hTuneDeviceName+"/PhaseC_Ref");
      hTuneModel = (INumberScalar)fftAttList.add(hTuneDeviceName+"/Tune");
      
      fftPanel.setHModel(hFFTMode, hFFTModeRef, hMaxIndexModel, hTrackSpanModel, hModeModel,hTuneModel);
      fftCPanel.setHModel(hFFTCMode, hFFTCModeRef, hMaxIndexModel, hTrackSpanModel, hModeModel,hTuneModel);
      phasePanel.setHModel(hPhaseMode, hPhaseModeRef, null, null, null, null);

      IEnumScalar   vModeModel = (IEnumScalar)fftRangeAttList.add(MainPanel.vTuneDeviceName+"/Mode");
      INumberScalar vMaxIndexModel = (INumberScalar)fftAttList.add(MainPanel.vTuneDeviceName+"/MaxIndex");
      INumberScalar vTrackSpanModel = (INumberScalar)fftAttList.add(MainPanel.vTuneDeviceName+"/TrackSpan");
      INumberSpectrum vFFTMode = (INumberSpectrum)fftAttList.add(MainPanel.vTuneDeviceName+"/FFT");
      INumberSpectrum vFFTModeRef = (INumberSpectrum)fftAttList.add(MainPanel.vTuneDeviceName+"/FFT_Ref");
      INumberSpectrum vFFTCMode = (INumberSpectrum)fftAttList.add(MainPanel.vTuneDeviceName+"/FFTC");
      INumberSpectrum vFFTCModeRef = (INumberSpectrum)fftAttList.add(MainPanel.vTuneDeviceName+"/FFTC_Ref");
      INumberSpectrum vPhaseMode = (INumberSpectrum)fftAttList.add(MainPanel.vTuneDeviceName+"/PhaseC");
      INumberSpectrum vPhaseModeRef = (INumberSpectrum)fftAttList.add(MainPanel.vTuneDeviceName+"/PhaseC_Ref");
      vTuneModel = (INumberScalar)fftAttList.add(vTuneDeviceName+"/Tune");

      fftPanel.setVModel(vFFTMode, vFFTModeRef, vMaxIndexModel, vTrackSpanModel, vModeModel,vTuneModel);
      fftCPanel.setVModel(vFFTCMode, vFFTCModeRef, vMaxIndexModel, vTrackSpanModel, vModeModel,vTuneModel);
      phasePanel.setVModel(vPhaseMode, vPhaseModeRef, null, null, null,null);
      
    } catch( ConnectionException e) {
      // Error message goes automatically to error window
    }
    
    fftPanel.refresh();
    fftCPanel.refresh();
    phasePanel.refresh();
            
    centerPanel.add(fftPanel);
    centerPanel.add(fftCPanel);
    centerPanel.add(phasePanel);

        
    try {
      
      
      IStringScalar cFileModel = (IStringScalar)attList.add(settingManagerName+"/LastAppliedFile");
      configFileScalarViewer.setBackgroundColor(Color.WHITE);
      configFileScalarViewer.setAlarmEnabled(true);
      configFileScalarViewer.setModel(cFileModel);
      
      IStringScalar hStatusModel = (IStringScalar)attList.add(hTuneDeviceName+"/ADCState");
      hAdcScalarViewer.setBackgroundColor(Color.WHITE);
      hAdcScalarViewer.setModel(hStatusModel);

      INumberScalar hSynchFreqModel = (INumberScalar)attList.add(hTuneDeviceName+"/SynchrotronFrequency");
      synchFreqViewer.setBackgroundColor(Color.WHITE);
      synchFreqViewer.setModel(hSynchFreqModel);
      
      IEnumScalar hSynch = (IEnumScalar)attList.add(hSparkDeviceName+"/SynchState");
      hSynchScalarViewer.setBackgroundColor(Color.WHITE);
      hSynchScalarViewer.setModel(hSynch);            
      
      IDevStateScalar hSparkStateModel = (IDevStateScalar)attList.add(hSparkDeviceName+"/State");
      hSparkStateViewer.setModel(hSparkStateModel);
      hSparkStateViewer.setStateClickable(false);
      hSparkStateViewer.addMouseListener(this);

      IStringScalar vStatusModel = (IStringScalar)attList.add(vTuneDeviceName+"/ADCState");
      vAdcScalarViewer.setBackgroundColor(Color.WHITE);
      vAdcScalarViewer.setModel(vStatusModel);
      
      IEnumScalar vSynch = (IEnumScalar)attList.add(vSparkDeviceName+"/SynchState");
      vSynchScalarViewer.setBackgroundColor(Color.WHITE);
      vSynchScalarViewer.setModel(vSynch);            
      
      IDevStateScalar vSparkStateModel = (IDevStateScalar)attList.add(vSparkDeviceName+"/State");
      vSparkStateViewer.setModel(vSparkStateModel);
      vSparkStateViewer.setStateClickable(false);
      vSparkStateViewer.addMouseListener(this);
      
      hTuneScalarViewer.setBackgroundColor(Color.WHITE);
      hTuneScalarViewer.setModel(hTuneModel);

      vTuneScalarViewer.setBackgroundColor(Color.WHITE);
      vTuneScalarViewer.setModel(vTuneModel);
      
      IEnumScalar hSourceModel = (IEnumScalar)attList.add(tuneSelectorName+"/Source_Qh");
      hSourceViewer.setAlarmEnabled(false);
      hSourceViewer.setBackgroundColor(Color.WHITE);
      hSourceViewer.setModel(hSourceModel);

      IEnumScalar vSourceModel = (IEnumScalar)attList.add(tuneSelectorName+"/Source_Qv");
      vSourceViewer.setAlarmEnabled(false);
      vSourceViewer.setBackgroundColor(Color.WHITE);
      vSourceViewer.setModel(vSourceModel);
      
    } catch( ConnectionException e) {
      // Error message goes automatically to error window
    }
    
    // Setting manager API
    sm = new SettingsManagerProxy(settingManagerName);
    sm.setErrorHistoryWindow(MainPanel.errWin);
          
    // Build the application menu
    try {

      DbDatum datum = db.get_property("JSrTune", "Applications");
      String[] apps = datum.extractStringArray();
      applications = new Vector<Apps>();
      
      for(int i=0;i<apps.length;i++) {
        
        String[] fields = apps[i].split(",");
        
        if(fields.length!=2) {
          JOptionPane.showMessageDialog(this, 
                  "Invalid application JSrTune propery (2 field expected)",
                  "Error",JOptionPane.ERROR_MESSAGE);
        } else {
          
          Apps a = new Apps();
          a.name = fields[0];
          a.command = fields[1];
          a.menuItem = new JMenuItem(a.name);
          a.menuItem.addActionListener(this);
          applications.add(a);
          applicationMenu.add(a.menuItem);
          
        }
        
        
        
      }

      
    } catch(DevFailed e) {
      
    }
    
    hFitFrame = new TuneFitFrame(hTuneDeviceName);

    fftAttList.setRefreshInterval(2000);
    fftAttList.startRefresher();
    fftRangeAttList.setRefreshInterval(1000);
    fftRangeAttList.startRefresher();
    attList.setRefreshInterval(1000);
    attList.startRefresher();
    
    splash.setVisible(false);
    setTitle("SR Tune " + APP_RELEASE + (isSpare?" [Spare]":""));
    ATKGraphicsUtils.centerFrameOnScreen(this);

    
  }
    
  public void resetPeakMarker() {
    setHMarkerCheckBox.setSelected(false);
    setVMarkerCheckBox.setSelected(false);
  }
  
  private void setPreset(int index) {
    
    try {
      DeviceProxy ds = new DeviceProxy(hTuneDeviceName);
      DeviceAttribute da = new DeviceAttribute("Preset");
      da.insert((short)index);
      ds.write_attribute(da);
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(centerPanel, hTuneDeviceName, e);
    }
            
    try {
      DeviceProxy ds = new DeviceProxy(vTuneDeviceName);
      DeviceAttribute da = new DeviceAttribute("Preset");
      da.insert((short)index);
      ds.write_attribute(da);
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(centerPanel, vTuneDeviceName, e);
    }
    
  }
  
  public void exitForm() {
    System.exit(0);
  }
  

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {
    java.awt.GridBagConstraints gridBagConstraints;

    upPanel = new javax.swing.JPanel();
    configPanel = new javax.swing.JPanel();
    configFileScalarViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    statusButton = new javax.swing.JButton();
    hSparkPanel = new javax.swing.JPanel();
    hSparkStateViewer = new fr.esrf.tangoatk.widget.attribute.SimpleStateViewer();
    hAdcScalarViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    hSynchScalarViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    vSparkPanel = new javax.swing.JPanel();
    vSparkStateViewer = new fr.esrf.tangoatk.widget.attribute.SimpleStateViewer();
    vAdcScalarViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    vSynchScalarViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    centerPanel = new javax.swing.JPanel();
    downPanel = new javax.swing.JPanel();
    commonPanel = new javax.swing.JPanel();
    refCurveCheckBox = new javax.swing.JCheckBox();
    memRefButton = new javax.swing.JButton();
    ResetAverageButton = new javax.swing.JButton();
    chTuneButton = new javax.swing.JButton();
    sensitivityPanel = new javax.swing.JPanel();
    PassiveButton = new javax.swing.JButton();
    FastButton = new javax.swing.JButton();
    HighSensitivityButton = new javax.swing.JButton();
    hPanel = new javax.swing.JPanel();
    hTuneLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    hTuneScalarViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    hSourceLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    hSourceViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    horizontalCheckBox = new javax.swing.JCheckBox();
    setHMarkerCheckBox = new javax.swing.JCheckBox();
    hSettingsButton = new javax.swing.JButton();
    vPanel = new javax.swing.JPanel();
    vTuneLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    vTuneScalarViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    vSourceLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    vSourceViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    verticalCheckBox = new javax.swing.JCheckBox();
    setVMarkerCheckBox = new javax.swing.JCheckBox();
    vSettingsButton = new javax.swing.JButton();
    jPanel1 = new javax.swing.JPanel();
    synchFreqViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    jMenuBar1 = new javax.swing.JMenuBar();
    jFileMenu = new javax.swing.JMenu();
    loadMenuItem = new javax.swing.JMenuItem();
    saveMenuItem = new javax.swing.JMenuItem();
    jSeparator2 = new javax.swing.JPopupMenu.Separator();
    exitMenuItem = new javax.swing.JMenuItem();
    viewMenu = new javax.swing.JMenu();
    FFT = new javax.swing.JMenu();
    frequencyMenuItem = new javax.swing.JRadioButtonMenuItem();
    fractionalMenuItem = new javax.swing.JRadioButtonMenuItem();
    Curves = new javax.swing.JMenu();
    displayFFTCheckBox = new javax.swing.JCheckBoxMenuItem();
    displayFFTCorrelatedCheckBox = new javax.swing.JCheckBoxMenuItem();
    displayPhaseCheckBox = new javax.swing.JCheckBoxMenuItem();
    rangeMenuItem = new javax.swing.JMenuItem();
    refreshMenuItem = new javax.swing.JCheckBoxMenuItem();
    decimateMenuItem = new javax.swing.JCheckBoxMenuItem();
    fitViewerMenuItem = new javax.swing.JMenuItem();
    jSeparator1 = new javax.swing.JPopupMenu.Separator();
    errorMenuItem = new javax.swing.JMenuItem();
    diagMenuItem = new javax.swing.JMenuItem();
    commandMenu = new javax.swing.JMenu();
    synchMenuItem = new javax.swing.JMenuItem();
    applicationMenu = new javax.swing.JMenu();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    upPanel.setLayout(new java.awt.GridBagLayout());

    configPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Configuration File", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
    configPanel.setLayout(new java.awt.GridBagLayout());

    configFileScalarViewer.setEditable(false);
    configFileScalarViewer.setText("simpleScalarViewer2");
    configFileScalarViewer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipady = 12;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.weightx = 1.0;
    gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 0);
    configPanel.add(configFileScalarViewer, gridBagConstraints);

    statusButton.setText("Status");
    statusButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        statusButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
    configPanel.add(statusButton, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.weightx = 1.0;
    upPanel.add(configPanel, gridBagConstraints);

    hSparkPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizontal Spark", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
    hSparkPanel.setLayout(new java.awt.GridBagLayout());

    hSparkStateViewer.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipadx = 50;
    gridBagConstraints.ipady = 12;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    hSparkPanel.add(hSparkStateViewer, gridBagConstraints);

    hAdcScalarViewer.setEditable(false);
    hAdcScalarViewer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    hAdcScalarViewer.setText("-------");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipadx = 50;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 3);
    hSparkPanel.add(hAdcScalarViewer, gridBagConstraints);

    hSynchScalarViewer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    hSynchScalarViewer.setText("-------");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipadx = 50;
    gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 3);
    hSparkPanel.add(hSynchScalarViewer, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    upPanel.add(hSparkPanel, gridBagConstraints);

    vSparkPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Vertical Spark", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
    vSparkPanel.setLayout(new java.awt.GridBagLayout());

    vSparkStateViewer.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipadx = 50;
    gridBagConstraints.ipady = 12;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 3, 3, 3);
    vSparkPanel.add(vSparkStateViewer, gridBagConstraints);

    vAdcScalarViewer.setEditable(false);
    vAdcScalarViewer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    vAdcScalarViewer.setText("-------");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipadx = 50;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 3);
    vSparkPanel.add(vAdcScalarViewer, gridBagConstraints);

    vSynchScalarViewer.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    vSynchScalarViewer.setText("-------");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipadx = 50;
    gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 3);
    vSparkPanel.add(vSynchScalarViewer, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    upPanel.add(vSparkPanel, gridBagConstraints);

    getContentPane().add(upPanel, java.awt.BorderLayout.NORTH);

    centerPanel.setLayout(new javax.swing.BoxLayout(centerPanel, javax.swing.BoxLayout.Y_AXIS));
    getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);

    downPanel.setLayout(new java.awt.GridBagLayout());

    commonPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Common", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
    commonPanel.setLayout(new java.awt.GridBagLayout());

    refCurveCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    refCurveCheckBox.setText("Display reference");
    refCurveCheckBox.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        refCurveCheckBoxActionPerformed(evt);
      }
    });
    commonPanel.add(refCurveCheckBox, new java.awt.GridBagConstraints());

    memRefButton.setText("Memorize reference");
    memRefButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        memRefButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
    commonPanel.add(memRefButton, gridBagConstraints);

    ResetAverageButton.setText("Reset Average");
    ResetAverageButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        ResetAverageButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
    commonPanel.add(ResetAverageButton, gridBagConstraints);

    chTuneButton.setText("Change Tune");
    chTuneButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        chTuneButtonActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
    commonPanel.add(chTuneButton, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    downPanel.add(commonPanel, gridBagConstraints);

    sensitivityPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Sensitivity", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N

    PassiveButton.setText("Passive");
    PassiveButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        PassiveButtonActionPerformed(evt);
      }
    });
    sensitivityPanel.add(PassiveButton);

    FastButton.setText("Fast");
    FastButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        FastButtonActionPerformed(evt);
      }
    });
    sensitivityPanel.add(FastButton);

    HighSensitivityButton.setText("High Sensitivity");
    HighSensitivityButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        HighSensitivityButtonActionPerformed(evt);
      }
    });
    sensitivityPanel.add(HighSensitivityButton);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    downPanel.add(sensitivityPanel, gridBagConstraints);

    hPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Horizontal", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
    hPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

    hTuneLabel.setOpaque(false);
    hTuneLabel.setText("Tune H");
    hPanel.add(hTuneLabel);

    hTuneScalarViewer.setForeground(java.awt.Color.red);
    hTuneScalarViewer.setText(" -.--- ");
    hTuneScalarViewer.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
    hTuneScalarViewer.setMargin(new java.awt.Insets(5, 20, 5, 20));
    hPanel.add(hTuneScalarViewer);

    hSourceLabel.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
    hSourceLabel.setText("Source");
    hPanel.add(hSourceLabel);

    hSourceViewer.setText("-----");
    hSourceViewer.setMargin(new java.awt.Insets(8, 20, 8, 20));
    hPanel.add(hSourceViewer);

    horizontalCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    horizontalCheckBox.setSelected(true);
    horizontalCheckBox.setText("Display");
    horizontalCheckBox.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        horizontalCheckBoxActionPerformed(evt);
      }
    });
    hPanel.add(horizontalCheckBox);

    setHMarkerCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    setHMarkerCheckBox.setForeground(new java.awt.Color(255, 0, 0));
    setHMarkerCheckBox.setText("Set Peak");
    setHMarkerCheckBox.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        setHMarkerCheckBoxActionPerformed(evt);
      }
    });
    hPanel.add(setHMarkerCheckBox);

    hSettingsButton.setText("Settings");
    hSettingsButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        hSettingsButtonActionPerformed(evt);
      }
    });
    hPanel.add(hSettingsButton);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    downPanel.add(hPanel, gridBagConstraints);

    vPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Vertical", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
    vPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

    vTuneLabel.setOpaque(false);
    vTuneLabel.setText("Tune V");
    vPanel.add(vTuneLabel);

    vTuneScalarViewer.setForeground(java.awt.Color.blue);
    vTuneScalarViewer.setText("-.---");
    vTuneScalarViewer.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
    vTuneScalarViewer.setMargin(new java.awt.Insets(5, 20, 5, 20));
    vPanel.add(vTuneScalarViewer);

    vSourceLabel.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
    vSourceLabel.setText("Source");
    vPanel.add(vSourceLabel);

    vSourceViewer.setText("-----");
    vSourceViewer.setMargin(new java.awt.Insets(8, 20, 8, 20));
    vPanel.add(vSourceViewer);

    verticalCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    verticalCheckBox.setSelected(true);
    verticalCheckBox.setText("Display");
    verticalCheckBox.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        verticalCheckBoxActionPerformed(evt);
      }
    });
    vPanel.add(verticalCheckBox);

    setVMarkerCheckBox.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
    setVMarkerCheckBox.setForeground(new java.awt.Color(0, 0, 255));
    setVMarkerCheckBox.setText("Set Peak");
    setVMarkerCheckBox.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        setVMarkerCheckBoxActionPerformed(evt);
      }
    });
    vPanel.add(setVMarkerCheckBox);

    vSettingsButton.setText("Settings");
    vSettingsButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        vSettingsButtonActionPerformed(evt);
      }
    });
    vPanel.add(vSettingsButton);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.gridwidth = 2;
    downPanel.add(vPanel, gridBagConstraints);

    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Synhrotron Freq.", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
    jPanel1.setLayout(new java.awt.GridBagLayout());

    synchFreqViewer.setText("----- Hz");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.ipadx = 50;
    gridBagConstraints.ipady = 10;
    jPanel1.add(synchFreqViewer, gridBagConstraints);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    downPanel.add(jPanel1, gridBagConstraints);

    getContentPane().add(downPanel, java.awt.BorderLayout.SOUTH);

    jFileMenu.setText("File");

    loadMenuItem.setText("Load...");
    loadMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        loadMenuItemActionPerformed(evt);
      }
    });
    jFileMenu.add(loadMenuItem);

    saveMenuItem.setText("Save...");
    saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        saveMenuItemActionPerformed(evt);
      }
    });
    jFileMenu.add(saveMenuItem);
    jFileMenu.add(jSeparator2);

    exitMenuItem.setText("Exit");
    exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        exitMenuItemActionPerformed(evt);
      }
    });
    jFileMenu.add(exitMenuItem);

    jMenuBar1.add(jFileMenu);

    viewMenu.setText("View");
    viewMenu.setToolTipText("");

    FFT.setText("FFT");

    frequencyMenuItem.setSelected(true);
    frequencyMenuItem.setText("Frequency");
    frequencyMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        frequencyMenuItemActionPerformed(evt);
      }
    });
    FFT.add(frequencyMenuItem);

    fractionalMenuItem.setText("Fractionnal");
    fractionalMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        fractionalMenuItemActionPerformed(evt);
      }
    });
    FFT.add(fractionalMenuItem);

    viewMenu.add(FFT);

    Curves.setText("Display");

    displayFFTCheckBox.setText("FFT");
    displayFFTCheckBox.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        displayFFTCheckBoxActionPerformed(evt);
      }
    });
    Curves.add(displayFFTCheckBox);

    displayFFTCorrelatedCheckBox.setSelected(true);
    displayFFTCorrelatedCheckBox.setText("Correlated FFT");
    displayFFTCorrelatedCheckBox.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        displayFFTCorrelatedCheckBoxActionPerformed(evt);
      }
    });
    Curves.add(displayFFTCorrelatedCheckBox);

    displayPhaseCheckBox.setText("Phase");
    displayPhaseCheckBox.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        displayPhaseCheckBoxActionPerformed(evt);
      }
    });
    Curves.add(displayPhaseCheckBox);

    viewMenu.add(Curves);

    rangeMenuItem.setText("Range...");
    rangeMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        rangeMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(rangeMenuItem);

    refreshMenuItem.setSelected(true);
    refreshMenuItem.setText("Refresh");
    refreshMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        refreshMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(refreshMenuItem);

    decimateMenuItem.setSelected(true);
    decimateMenuItem.setText("Decimate (display)");
    decimateMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        decimateMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(decimateMenuItem);

    fitViewerMenuItem.setText("Fitting Info...");
    fitViewerMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        fitViewerMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(fitViewerMenuItem);
    viewMenu.add(jSeparator1);

    errorMenuItem.setText("Errors...");
    errorMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        errorMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(errorMenuItem);

    diagMenuItem.setText("Diagnostics...");
    diagMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        diagMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(diagMenuItem);

    jMenuBar1.add(viewMenu);

    commandMenu.setText("Commands");

    synchMenuItem.setText("Synchronize...");
    synchMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        synchMenuItemActionPerformed(evt);
      }
    });
    commandMenu.add(synchMenuItem);

    jMenuBar1.add(commandMenu);

    applicationMenu.setText("Applications");
    jMenuBar1.add(applicationMenu);

    setJMenuBar(jMenuBar1);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
    exitForm();
  }//GEN-LAST:event_exitMenuItemActionPerformed

  private void errorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorMenuItemActionPerformed
    ATKGraphicsUtils.centerFrameOnScreen(errWin);
    errWin.setVisible(true);    
  }//GEN-LAST:event_errorMenuItemActionPerformed

  private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
    ATKDiagnostic.showDiagnostic();
  }//GEN-LAST:event_diagMenuItemActionPerformed

  private void frequencyMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_frequencyMenuItemActionPerformed
    fftPanel.setXAxisMode(TuneViewer.FREQUENCY);        
    fftCPanel.setXAxisMode(TuneViewer.FREQUENCY);        
    fractionalMenuItem.setSelected(false);
  }//GEN-LAST:event_frequencyMenuItemActionPerformed

  private void fractionalMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fractionalMenuItemActionPerformed
    fftPanel.setXAxisMode(TuneViewer.FRACTIONNAL);        
    fftCPanel.setXAxisMode(TuneViewer.FRACTIONNAL);        
    frequencyMenuItem.setSelected(false);
  }//GEN-LAST:event_fractionalMenuItemActionPerformed

  private void refreshMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshMenuItemActionPerformed
    boolean r = refreshMenuItem.isSelected();
    if(r)
      fftAttList.startRefresher();
    else
      fftAttList.stopRefresher();
  }//GEN-LAST:event_refreshMenuItemActionPerformed

  private void loadMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadMenuItemActionPerformed
      sm.loadSettingsFile();
  }//GEN-LAST:event_loadMenuItemActionPerformed

  private void horizontalCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_horizontalCheckBoxActionPerformed
    fftPanel.setHVisible(horizontalCheckBox.isSelected());          
    fftCPanel.setHVisible(horizontalCheckBox.isSelected());
    phasePanel.setHVisible(horizontalCheckBox.isSelected());
  }//GEN-LAST:event_horizontalCheckBoxActionPerformed

  private void verticalCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verticalCheckBoxActionPerformed
    fftPanel.setVVisible(verticalCheckBox.isSelected());            
    fftCPanel.setVVisible(verticalCheckBox.isSelected());
    phasePanel.setVVisible(verticalCheckBox.isSelected());
  }//GEN-LAST:event_verticalCheckBoxActionPerformed

  private void setVMarkerCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setVMarkerCheckBoxActionPerformed
    fftPanel.setVPeak();
    fftCPanel.setVPeak();    
  }//GEN-LAST:event_setVMarkerCheckBoxActionPerformed

  private void setHMarkerCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setHMarkerCheckBoxActionPerformed
    fftPanel.setHPeak();
    fftCPanel.setHPeak();
  }//GEN-LAST:event_setHMarkerCheckBoxActionPerformed

  private void hSettingsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hSettingsButtonActionPerformed

    if(hFrame==null)
      hFrame = new SettingsFrame("Horizontal",hTuneDeviceName);
    hFrame.showDialog(this);
    
  }//GEN-LAST:event_hSettingsButtonActionPerformed

  private void vSettingsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vSettingsButtonActionPerformed

    if(vFrame==null)
      vFrame = new SettingsFrame("Vertical",vTuneDeviceName);
    vFrame.showDialog(this);
    
  }//GEN-LAST:event_vSettingsButtonActionPerformed

  private void rangeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rangeMenuItemActionPerformed
    rFrame.showDialog();
  }//GEN-LAST:event_rangeMenuItemActionPerformed

  private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
    sm.saveSettingsFile();
  }//GEN-LAST:event_saveMenuItemActionPerformed

  private void refCurveCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refCurveCheckBoxActionPerformed
    fftPanel.setRefVisible(refCurveCheckBox.isSelected());
    fftCPanel.setRefVisible(refCurveCheckBox.isSelected());
    phasePanel.setRefVisible(refCurveCheckBox.isSelected());
  }//GEN-LAST:event_refCurveCheckBoxActionPerformed

  private void statusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statusButtonActionPerformed

    try {

      String status;
      DeviceAttribute da = sm.getDevice().read_attribute("Status");
      status = da.extractString();
      JOptionPane.showMessageDialog(this,status,"Status ["+settingManagerName+"]",JOptionPane.INFORMATION_MESSAGE);

    } catch (DevFailed ex) {
      ErrorPane.showErrorMessage(this, settingManagerName, ex);
    }

  }//GEN-LAST:event_statusButtonActionPerformed

  private void ResetAverageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetAverageButtonActionPerformed
    
    try {
      DeviceProxy ds = new DeviceProxy(hTuneDeviceName);
      ds.command_inout("ResetAverage");
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(centerPanel, hTuneDeviceName, e);
    }
            
    try {
      DeviceProxy ds = new DeviceProxy(vTuneDeviceName);
      ds.command_inout("ResetAverage");
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(centerPanel, vTuneDeviceName, e);
    }           
    
  }//GEN-LAST:event_ResetAverageButtonActionPerformed

  private void PassiveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PassiveButtonActionPerformed
    setPreset(1);
  }//GEN-LAST:event_PassiveButtonActionPerformed

  private void FastButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FastButtonActionPerformed
    setPreset(2);
  }//GEN-LAST:event_FastButtonActionPerformed

  private void HighSensitivityButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HighSensitivityButtonActionPerformed

    boolean isUSM = false;
    
    try {
      DeviceProxy ds = new DeviceProxy(machstatName);
      DeviceAttribute da = ds.read_attribute("Sr_mode");
      isUSM = da.extractShort() == 1;
    } catch(DevFailed e) {
      ErrorPane.showErrorMessage(centerPanel, machstatName, e);
      return;
    }

    if(isUSM) {
      int r = JOptionPane.showConfirmDialog(centerPanel,
              "Warning, High sensitivity may affect the beam in USM mode\nDo you want to proceed ?",
              "Confirmation",JOptionPane.YES_NO_OPTION,JOptionPane.INFORMATION_MESSAGE);
      if(r!=JOptionPane.YES_OPTION)
        return;
    }
    
    setPreset(3);
  }//GEN-LAST:event_HighSensitivityButtonActionPerformed

  private void displayFFTCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_displayFFTCheckBoxActionPerformed
    fftPanel.setVisible(displayFFTCheckBox.isSelected());
    pack();
  }//GEN-LAST:event_displayFFTCheckBoxActionPerformed

  private void displayPhaseCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_displayPhaseCheckBoxActionPerformed
    // TODO add your handling code here:
    phasePanel.setVisible(displayPhaseCheckBox.isSelected());
    pack();
  }//GEN-LAST:event_displayPhaseCheckBoxActionPerformed

  private void displayFFTCorrelatedCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_displayFFTCorrelatedCheckBoxActionPerformed
    fftCPanel.setVisible(displayFFTCorrelatedCheckBox.isSelected());
    pack();
  }//GEN-LAST:event_displayFFTCorrelatedCheckBoxActionPerformed

  private void synchMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_synchMenuItemActionPerformed

    if(synchFrame==null) {
      synchFrame = new SynchFrame();
      ATKGraphicsUtils.centerFrameOnScreen(synchFrame);
    }
    synchFrame.setVisible(true);
        
  }//GEN-LAST:event_synchMenuItemActionPerformed

  private void memRefButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_memRefButtonActionPerformed

    // Take ref
    fftPanel.takeRef();
    fftCPanel.takeRef();
    phasePanel.takeRef();

  }//GEN-LAST:event_memRefButtonActionPerformed

  private void chTuneButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chTuneButtonActionPerformed

    if(tFrame==null)
      tFrame = new TuneAdjustFrame();
    tFrame.showDialog(this);

  }//GEN-LAST:event_chTuneButtonActionPerformed

  private void fitViewerMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fitViewerMenuItemActionPerformed
    hFitFrame.showFrame();
  }//GEN-LAST:event_fitViewerMenuItemActionPerformed

  private void decimateMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decimateMenuItemActionPerformed
    doDecimation = decimateMenuItem.isSelected();
  }//GEN-LAST:event_decimateMenuItemActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    
    if(args.length==0) {
      
      hTuneDeviceName = "srdiag/tm/spark-h";
      vTuneDeviceName = "srdiag/tm/spark-v";
      settingManagerName = "sys/settings/sr-tune";
      new MainPanel(true,false).setVisible(true);         
              
    } else {
    
      if(args.length!=3) {
        System.out.println("Usage: jsrtune hTune vTune sManager");
        System.exit(0);
      }
    
      hTuneDeviceName = args[0];
      vTuneDeviceName = args[1];
      settingManagerName = args[2];
      boolean isSpare = settingManagerName.toLowerCase().endsWith("spare");
    
      new MainPanel(true,isSpare).setVisible(true);
      
    }
    
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JMenu Curves;
  private javax.swing.JMenu FFT;
  private javax.swing.JButton FastButton;
  private javax.swing.JButton HighSensitivityButton;
  private javax.swing.JButton PassiveButton;
  private javax.swing.JButton ResetAverageButton;
  private javax.swing.JMenu applicationMenu;
  private javax.swing.JPanel centerPanel;
  private javax.swing.JButton chTuneButton;
  private javax.swing.JMenu commandMenu;
  private javax.swing.JPanel commonPanel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer configFileScalarViewer;
  private javax.swing.JPanel configPanel;
  private javax.swing.JCheckBoxMenuItem decimateMenuItem;
  private javax.swing.JMenuItem diagMenuItem;
  private javax.swing.JCheckBoxMenuItem displayFFTCheckBox;
  private javax.swing.JCheckBoxMenuItem displayFFTCorrelatedCheckBox;
  private javax.swing.JCheckBoxMenuItem displayPhaseCheckBox;
  private javax.swing.JPanel downPanel;
  private javax.swing.JMenuItem errorMenuItem;
  private javax.swing.JMenuItem exitMenuItem;
  private javax.swing.JMenuItem fitViewerMenuItem;
  private javax.swing.JRadioButtonMenuItem fractionalMenuItem;
  private javax.swing.JRadioButtonMenuItem frequencyMenuItem;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer hAdcScalarViewer;
  private javax.swing.JPanel hPanel;
  private javax.swing.JButton hSettingsButton;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel hSourceLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer hSourceViewer;
  private javax.swing.JPanel hSparkPanel;
  private fr.esrf.tangoatk.widget.attribute.SimpleStateViewer hSparkStateViewer;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer hSynchScalarViewer;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel hTuneLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer hTuneScalarViewer;
  private javax.swing.JCheckBox horizontalCheckBox;
  private javax.swing.JMenu jFileMenu;
  private javax.swing.JMenuBar jMenuBar1;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPopupMenu.Separator jSeparator1;
  private javax.swing.JPopupMenu.Separator jSeparator2;
  private javax.swing.JMenuItem loadMenuItem;
  private javax.swing.JButton memRefButton;
  private javax.swing.JMenuItem rangeMenuItem;
  private javax.swing.JCheckBox refCurveCheckBox;
  private javax.swing.JCheckBoxMenuItem refreshMenuItem;
  private javax.swing.JMenuItem saveMenuItem;
  private javax.swing.JPanel sensitivityPanel;
  private javax.swing.JCheckBox setHMarkerCheckBox;
  private javax.swing.JCheckBox setVMarkerCheckBox;
  private javax.swing.JButton statusButton;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer synchFreqViewer;
  private javax.swing.JMenuItem synchMenuItem;
  private javax.swing.JPanel upPanel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer vAdcScalarViewer;
  private javax.swing.JPanel vPanel;
  private javax.swing.JButton vSettingsButton;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel vSourceLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer vSourceViewer;
  private javax.swing.JPanel vSparkPanel;
  private fr.esrf.tangoatk.widget.attribute.SimpleStateViewer vSparkStateViewer;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer vSynchScalarViewer;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel vTuneLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer vTuneScalarViewer;
  private javax.swing.JCheckBox verticalCheckBox;
  private javax.swing.JMenu viewMenu;
  // End of variables declaration//GEN-END:variables

  @Override
  public void mouseClicked(MouseEvent e) {
    if(e.getSource()==hSparkStateViewer) {
      new atkpanel.MainPanel(hSparkDeviceName,false);
    } else if(e.getSource()==vSparkStateViewer) {
      new atkpanel.MainPanel(vSparkDeviceName,false);
    }
  }

  @Override
  public void mousePressed(MouseEvent e) {
  }

  @Override
  public void mouseReleased(MouseEvent e) {
  }

  @Override
  public void mouseEntered(MouseEvent e) {
  }

  @Override
  public void mouseExited(MouseEvent e) {
  }
  
  @Override
  public void actionPerformed(ActionEvent e) {
    
    Object src = e.getSource();
    
    boolean found = false;
    int i = 0;
    while(!found && i<applications.size()) {
      found = applications.get(i).menuItem == src;
      if(!found) i++;
    }
    
    if(found) {
      Apps a = applications.get(i);
      try {
        Runtime.getRuntime().exec(a.command);
      } catch (IOException ex) {
        JOptionPane.showMessageDialog(this, 
                "Cannot launch " + a.command + "\n" + ex.getMessage(),
                "Error",JOptionPane.ERROR_MESSAGE);
      }      
    }
    
  }

          
}
