/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRTune;

import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.INumberScalarListener;
import fr.esrf.tangoatk.core.NumberScalarEvent;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.JSmoothLabel;
import javax.swing.JFrame;

/**
 *
 * @author pons
 */
public class RangeFrame extends javax.swing.JFrame implements INumberScalarListener {

  /**
   * Creates new form RangePanel
   */
  MainPanel parent;
  INumberScalar maxXModel;
  INumberScalar minXModel;
 
  public RangeFrame(MainPanel parent) {
    
    this.parent = parent;
    initComponents();
    setTitle("Display range");
    setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    
  }
  
  public void setScaleModel(INumberScalar minX,INumberScalar maxX,INumberScalar minY,INumberScalar maxY) {

    minXWheelEditor.setModel(minX);
    minYWheelEditor.setModel(minY);
    maxXWheelEditor.setModel(maxX);
    maxYWheelEditor.setModel(maxY);
    minXModel = minX;
    maxXModel = maxX;
    minX.addNumberScalarListener(this);
    maxX.addNumberScalarListener(this);
        
  }
  
  public void clearModel() {
    
    minXWheelEditor.setModel(null);
    minYWheelEditor.setModel(null);
    maxXWheelEditor.setModel(null);
    maxYWheelEditor.setModel(null);
    minXModel.removeNumberScalarListener(this);
    maxXModel.removeNumberScalarListener(this);
    
  }    
  
  public void showDialog() {
    
    if(!isVisible()) {
      ATKGraphicsUtils.centerFrame(parent.getRootPane(), this);
    }
    setVisible(true);
    
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {
    java.awt.GridBagConstraints gridBagConstraints;

    centerPanel = new javax.swing.JPanel();
    minLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    maxLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    minXWheelEditor = new fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor();
    maxXWheelEditor = new fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor();
    minLabel1 = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    minYWheelEditor = new fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor();
    maxLabel1 = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    maxYWheelEditor = new fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor();
    ratioMinLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    ratioMaxLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    btnPanel = new javax.swing.JPanel();
    dismissButton = new javax.swing.JButton();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

    centerPanel.setLayout(new java.awt.GridBagLayout());

    minLabel.setOpaque(false);
    minLabel.setText("Min frequnecy");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(5, 5, 3, 0);
    centerPanel.add(minLabel, gridBagConstraints);

    maxLabel.setOpaque(false);
    maxLabel.setText("Max frequnecy");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 0);
    centerPanel.add(maxLabel, gridBagConstraints);

    minXWheelEditor.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
    centerPanel.add(minXWheelEditor, gridBagConstraints);

    maxXWheelEditor.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
    centerPanel.add(maxXWheelEditor, gridBagConstraints);

    minLabel1.setOpaque(false);
    minLabel1.setText("Min amplitude");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 2;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(5, 5, 3, 0);
    centerPanel.add(minLabel1, gridBagConstraints);

    minYWheelEditor.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 2;
    gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
    centerPanel.add(minYWheelEditor, gridBagConstraints);

    maxLabel1.setOpaque(false);
    maxLabel1.setText("Max amplitude");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 3;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(0, 5, 5, 0);
    centerPanel.add(maxLabel1, gridBagConstraints);

    maxYWheelEditor.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 3;
    gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
    centerPanel.add(maxYWheelEditor, gridBagConstraints);

    ratioMinLabel.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
    ratioMinLabel.setText("-.---");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipadx = 40;
    gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
    centerPanel.add(ratioMinLabel, gridBagConstraints);

    ratioMaxLabel.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
    ratioMaxLabel.setText("-.---");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.ipadx = 40;
    centerPanel.add(ratioMaxLabel, gridBagConstraints);

    getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);

    btnPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

    dismissButton.setText("Dismiss");
    dismissButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        dismissButtonActionPerformed(evt);
      }
    });
    btnPanel.add(dismissButton);

    getContentPane().add(btnPanel, java.awt.BorderLayout.SOUTH);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void dismissButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dismissButtonActionPerformed
    setVisible(false);
  }//GEN-LAST:event_dismissButtonActionPerformed

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel btnPanel;
  private javax.swing.JPanel centerPanel;
  private javax.swing.JButton dismissButton;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel maxLabel;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel maxLabel1;
  private fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor maxXWheelEditor;
  private fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor maxYWheelEditor;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel minLabel;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel minLabel1;
  private fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor minXWheelEditor;
  private fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor minYWheelEditor;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel ratioMaxLabel;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel ratioMinLabel;
  // End of variables declaration//GEN-END:variables


  private void setRatio(JSmoothLabel label,double freq) {  

    if(Double.isNaN(freq))
      label.setText("-.---");
    else
      label.setText(String.format("%.3f",freq*1000.0/SRTune.TuneViewer.REVOLUTION_FREQ));
    
  }

  @Override
  public void numberScalarChange(NumberScalarEvent nse) {

    if(nse.getNumberSource()==minXModel) {
      setRatio(ratioMinLabel,nse.getValue());
    } else if (nse.getNumberSource()==maxXModel) {
      setRatio(ratioMaxLabel,nse.getValue());
    }

  }

  @Override
  public void stateChange(AttributeStateEvent ase) {
  }

  @Override
  public void errorChange(ErrorEvent ee) {
    if(ee.getSource()==minXModel) {
      setRatio(ratioMinLabel,Double.NaN);
    } else if (ee.getSource()==maxXModel) {
      setRatio(ratioMaxLabel,Double.NaN);
    }
  }
  
}
