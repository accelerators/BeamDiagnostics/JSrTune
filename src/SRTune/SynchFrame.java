/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRTune;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import java.awt.Dimension;

/**
 *
 * @author pons
 */
public class SynchFrame extends javax.swing.JFrame {

  final static String[] SYNCH_STATE = {"NoSynch","Tracking","Synchronized"};
  
  private Thread    sequenceThread=null;

  /**
   * Creates new form SynchFrame
   */
  public SynchFrame() {    
    initComponents();    
    scrollPane.setPreferredSize(new Dimension(600,400));
    setTitle("Spark Tune Synchronistation");    
  }
  
  public void setSequenceText(String msg) {

    sequenceText.setText(msg);
    sequenceText.setCaretPosition(msg.length());

  }
  
  public void synchronize(String devName) {

    final SynchFrame theDlg = this;
    final String sparkDevName = devName;

    sequenceThread = new Thread() {

      StringBuffer    seqBuffer = new StringBuffer();


      public void run() {

        DeviceProxy sparkDS;

        seqBuffer.append("Starting Synchronize seuquence of "+devName+"\n");
        setSequenceText(seqBuffer.toString());

        // -- Import ---------------------------------------------------------

        try {
          sparkDS = new DeviceProxy(sparkDevName);
        } catch (DevFailed e) {
          seqBuffer.append("Sequence failed, cannot import "+sparkDevName+"\n");
          seqBuffer.append(e.errors[0].desc);
          setSequenceText(seqBuffer.toString());
          launchHButton.setEnabled(true);
          launchVButton.setEnabled(true);
          return;
        }

        seqBuffer.append(sparkDevName + " successfully imported\n");
        
        // ------------------------------------------------------------------
        seqBuffer.append("Set trigger delay to 0\n");
        setSequenceText(seqBuffer.toString());

        try {
          DeviceAttribute da = new DeviceAttribute("TriggerDelay");
          da.insert_ul(0);          
          sparkDS.write_attribute(da);
        } catch (DevFailed e) {
          seqBuffer.append("Sequence failed, Set TriggerDelay on "+sparkDevName+" failed\n");
          seqBuffer.append(e.errors[0].desc);
          setSequenceText(seqBuffer.toString());
          launchHButton.setEnabled(true);
          launchVButton.setEnabled(true);
          return;
        }
        
        // ------------------------------------------------------------------
        String[] attList1 = new String[] {"ADC","TBT","TBT_IQ","TBT_Dec","TDP","TDP_Dec"};
        String[] attList2 = new String[] {"TBT_Window","SA_History"};
        boolean[] enaList1 = new boolean[attList1.length];
        boolean[] enaList2 = new boolean[attList2.length];
        
        seqBuffer.append("Memorized enable flags\n");
        setSequenceText(seqBuffer.toString());
        
        try {

          for(int i=0;i<attList1.length;i++) {
            DeviceAttribute da = sparkDS.read_attribute(attList1[i]+"_Enable");
            enaList1[i] = da.extractBoolean();
            seqBuffer.append(attList1[i] + "_Enable: " + enaList1[i] +"\n");
            setSequenceText(seqBuffer.toString());
          }
          for(int i=0;i<attList2.length;i++) {
            DeviceAttribute da = sparkDS.read_attribute(attList2[i]+"_Enable");
            enaList2[i] = da.extractBoolean();
            seqBuffer.append(attList2[i] + "_Enable: " + enaList2[i] +"\n");
          }
          
        } catch (DevFailed e) {
          seqBuffer.append("Sequence failed, Attribute reading of "+sparkDevName+" failed\n");
          seqBuffer.append(e.errors[0].desc);
          setSequenceText(seqBuffer.toString());
          launchHButton.setEnabled(true);
          launchVButton.setEnabled(true);
          return;
        }
        
        // ------------------------------------------------------------------
        seqBuffer.append("Disable flags\n");
        setSequenceText(seqBuffer.toString());
        
        try {

          for(int i=0;i<attList1.length;i++) {
            DeviceAttribute da = new DeviceAttribute(attList1[i]+"_Enable");
            da.insert(false);
            sparkDS.write_attribute(da);
            seqBuffer.append(attList1[i] + "_Enable: false\n");
            setSequenceText(seqBuffer.toString());
          }
          for(int i=0;i<attList2.length;i++) {
            DeviceAttribute da = new DeviceAttribute(attList2[i]+"_Enable");
            da.insert(false);
            sparkDS.write_attribute(da);
            seqBuffer.append(attList2[i] + "_Enable: false\n");
          }
          
        } catch (DevFailed e) {
          seqBuffer.append("Sequence failed, Attribute disabling of "+sparkDevName+" failed\n");
          seqBuffer.append(e.errors[0].desc);
          setSequenceText(seqBuffer.toString());
          launchHButton.setEnabled(true);
          launchVButton.setEnabled(true);
          return;
        }
        
        // ------------------------------------------------------------------
        seqBuffer.append("Initialise mode\n");
        setSequenceText(seqBuffer.toString());
        
        try {

          for(int i=0;i<attList1.length;i++) {
            DeviceAttribute da = new DeviceAttribute(attList1[i]+"_Mode");
            da.insert((int)3);
            sparkDS.write_attribute(da);
            seqBuffer.append(attList1[i] + "_Mode: DOD On Event\n");
            setSequenceText(seqBuffer.toString());
          }
          for(int i=0;i<attList2.length;i++) {
            DeviceAttribute da = new DeviceAttribute(attList2[i]+"_Mode");
            da.insert((int)1);
            sparkDS.write_attribute(da);
            seqBuffer.append(attList2[i] + "_Enable: DOD Now\n");
          }
          
        } catch (DevFailed e) {
          seqBuffer.append("Sequence failed, Mode initialisation of "+sparkDevName+" failed\n");
          seqBuffer.append(e.errors[0].desc);
          setSequenceText(seqBuffer.toString());
          launchHButton.setEnabled(true);
          launchVButton.setEnabled(true);
          return;
        }
        
        // ------------------------------------------------------------------
        seqBuffer.append("Start synchronisation\n");
        setSequenceText(seqBuffer.toString());
        
        try {

          DeviceAttribute da = new DeviceAttribute("Synchronize");
          da.insert_u64(0);
          sparkDS.write_attribute(da);
          seqBuffer.append("Synchronize: Set to 0\n");
          setSequenceText(seqBuffer.toString());
          
        } catch (DevFailed e) {
          seqBuffer.append("Sequence failed, Synchronize of "+sparkDevName+" failed\n");
          seqBuffer.append(e.errors[0].desc);
          setSequenceText(seqBuffer.toString());
          launchHButton.setEnabled(true);
          launchVButton.setEnabled(true);
          return;
        }
        
        // ------------------------------------------------------------------
        seqBuffer.append("Wait for synchronisation\n");
        setSequenceText(seqBuffer.toString());
        
        try {

          long v = 0;
          while(v!=2) {
            try{ Thread.sleep(1000); } catch (InterruptedException e) {}
            DeviceAttribute da = sparkDS.read_attribute("SynchState");
            v = da.extractLong64();
            seqBuffer.append("Wait for synchronisation :" + SYNCH_STATE[(int)v] + "\n");
            setSequenceText(seqBuffer.toString());            
          }
          seqBuffer.append("Wait for synchronisation :" + SYNCH_STATE[(int)v] + "\n");
          setSequenceText(seqBuffer.toString());            
          
          
        } catch (DevFailed e) {
          seqBuffer.append("Sequence failed, Synchronize of "+sparkDevName+" failed\n");
          seqBuffer.append(e.errors[0].desc);
          setSequenceText(seqBuffer.toString());
          launchHButton.setEnabled(true);
          launchVButton.setEnabled(true);
          return;
        }
        
        // ------------------------------------------------------------------
        seqBuffer.append("Restore flags\n");
        setSequenceText(seqBuffer.toString());
        
        try {

          for(int i=0;i<attList1.length;i++) {
            DeviceAttribute da = new DeviceAttribute(attList1[i]+"_Enable");
            da.insert(enaList1[i]);
            sparkDS.write_attribute(da);
            seqBuffer.append(attList1[i] + "_Enable: "+Boolean.toString(enaList1[i])+"\n");
            setSequenceText(seqBuffer.toString());
          }
          for(int i=0;i<attList2.length;i++) {
            DeviceAttribute da = new DeviceAttribute(attList2[i]+"_Enable");
            da.insert(enaList2[i]);
            sparkDS.write_attribute(da);
            seqBuffer.append(attList2[i] + "_Enable: "+Boolean.toString(enaList2[i])+"\n");
          }
          
        } catch (DevFailed e) {
          seqBuffer.append("Sequence failed, Attribute writing of "+sparkDevName+" failed\n");
          seqBuffer.append(e.errors[0].desc);
          setSequenceText(seqBuffer.toString());
          launchHButton.setEnabled(true);
          launchVButton.setEnabled(true);
          return;
        }
        
        seqBuffer.append("Synchronization sequence done\n");
        setSequenceText(seqBuffer.toString());
          launchHButton.setEnabled(true);
        launchVButton.setEnabled(true);

      }

    };
    sequenceThread.start();

  }
  

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    scrollPane = new javax.swing.JScrollPane();
    sequenceText = new javax.swing.JTextArea();
    downPanel = new javax.swing.JPanel();
    launchHButton = new javax.swing.JButton();
    launchVButton = new javax.swing.JButton();
    cancelButtom = new javax.swing.JButton();

    sequenceText.setEditable(false);
    sequenceText.setColumns(20);
    sequenceText.setRows(5);
    scrollPane.setViewportView(sequenceText);

    getContentPane().add(scrollPane, java.awt.BorderLayout.CENTER);

    downPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

    launchHButton.setText("Synch Horz");
    launchHButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        launchHButtonActionPerformed(evt);
      }
    });
    downPanel.add(launchHButton);

    launchVButton.setText("Synch Vert");
    launchVButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        launchVButtonActionPerformed(evt);
      }
    });
    downPanel.add(launchVButton);

    cancelButtom.setText("Cancel");
    cancelButtom.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        cancelButtomActionPerformed(evt);
      }
    });
    downPanel.add(cancelButtom);

    getContentPane().add(downPanel, java.awt.BorderLayout.SOUTH);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void cancelButtomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtomActionPerformed
    if(sequenceThread!=null) sequenceThread.interrupt();
    setVisible(false);
  }//GEN-LAST:event_cancelButtomActionPerformed

  private void launchVButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchVButtonActionPerformed
    launchVButton.setEnabled(false);
    launchHButton.setEnabled(false);
    synchronize(MainPanel.vSparkDeviceName);
  }//GEN-LAST:event_launchVButtonActionPerformed

  private void launchHButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_launchHButtonActionPerformed
    launchHButton.setEnabled(false);
    launchVButton.setEnabled(false);
    synchronize(MainPanel.hSparkDeviceName);
  }//GEN-LAST:event_launchHButtonActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(SynchFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(SynchFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(SynchFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(SynchFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new SynchFrame().setVisible(true);
      }
    });
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton cancelButtom;
  private javax.swing.JPanel downPanel;
  private javax.swing.JButton launchHButton;
  private javax.swing.JButton launchVButton;
  private javax.swing.JScrollPane scrollPane;
  private javax.swing.JTextArea sequenceText;
  // End of variables declaration//GEN-END:variables
}
